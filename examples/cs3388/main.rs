use crate::graphics_window::GraphicsWindow;
use graphics::transform::*;
use graphics::*;

mod graphics_window;

fn main() {
    let start_time = std::time::SystemTime::now();
    asn3();
    println!("{:?}", start_time.elapsed().unwrap());
    asn4();
    println!("{:?}", start_time.elapsed().unwrap());
}

fn asn3() {
    let width = 1800;
    let height = 840;
    let theta = 45.0_f64.to_radians();

    // Up direction
    let up = Vector::new(0.0, 0.0, 1.0);

    // Origin of viewing coordinates
    let e = Point::new(120.0, 120.0, 40.0);

    // Gaze point
    let g = Point::new(0.0, 0.0, -40.0);

    // Light position
    let light_position = Point::new(10.0, 10.0, 40.0);

    // Light intensity
    let light_intensity = [1.0, 1.0, 1.0];

    // BEGIN PROGRAM HERE

    // rotation for the cylinder, its caps, and the torus around (1,1,1) by a non-zero angle
    let r1 = rotate(Vector::new(1.0, 1.0, 1.0), 45.0_f64.to_radians());

    // rotation to flip the bottom circle so it points in the right direction
    let r2 = rotate_x(180.0_f64.to_radians());

    let reflectance = Reflectance {
        ambient: 0.2,
        diffuse: 0.4,
        specular: 0.4,
        specular_exponent: 10.0,
    };
    let shapes = vec![
        Object::new(
            Mesh::new(Shape::Plane, 1.0 / 100.0, 1.0 / 100.0),
            Material {
                color: [255, 0, 255],
                reflectance,
            },
            scale(100.0, 100.0, 100.0).translate(-40.0, -40.0, -40.0),
        ),
        Object::new(
            Mesh::new(Shape::Cone, 1.0 / 100.0, 1.0 / 360.0),
            Material {
                color: [0, 255, 0],
                reflectance,
            },
            rotate_x(180.0_f64.to_radians())
                .scale(10.0, 10.0, 20.0)
                .translate(50.0, 0.0, 20.0),
        ),
        Object::new(
            Mesh::new(Shape::Sphere, 1.0 / 1280.0, 1.0 / 640.0),
            Material {
                color: [255, 255, 0],
                reflectance,
            },
            scale(10.0, 10.0, 10.0).translate_x(30.0),
        ),
        Object::new(
            Mesh::new(Shape::Cylinder, 1.0 / 100.0, 1.0 / 360.0),
            Material {
                color: [255, 0, 0],
                reflectance,
            },
            scale(10.0, 10.0, 20.0).transform(r1),
        ),
        // top circle
        Object::new(
            Mesh::new(Shape::Circle, 1.0 / 100.0, 1.0 / 360.0),
            Material {
                color: [255, 0, 0],
                reflectance,
            },
            scale(10.0, 10.0, 10.0).translate_z(20.0).transform(r1),
        ),
        // bottom circle
        Object::new(
            Mesh::new(Shape::Circle, 1.0 / 100.0, 1.0 / 180.0),
            Material {
                color: [255, 0, 0],
                reflectance,
            },
            scale(10.0, 10.0, 10.0).transform(r2).transform(r1),
        ),
        Object::new(
            Mesh::new(Shape::Torus(0.25), 1.0 / 2560.0, 1.0 / 640.0),
            Material {
                color: [0, 255, 0],
                reflectance,
            },
            scale(20.0, 20.0, 20.0).translate_x(50.0).transform(r1),
        ),
    ];

    let camera = Camera::new(up, e, g, width, height, theta);
    let light = LightSource {
        position: light_position,
        intensity: light_intensity,
    };

    let mut window = graphics_window::GraphicsWindow::new(width, height);
    window.draw_faces(shade_faces(&shapes, &camera, &light));
    window.save_image("images/image1a.png");

    let window = ray_trace(width, height, &shapes, &camera, &light);
    window.save_image("images/image1b.png");
}

fn asn4() {
    let w = 512;
    let h = 512;
    let theta = 45.0_f64.to_radians();

    let u = Vector::new(0.0, 0.0, 1.0);
    let e = Point::new(5.0, 5.0, 5.0);
    let g = Point::new(0.0, 0.0, 0.0);

    let camera = Camera::new(u, e, g, w, h, theta);
    let light = LightSource {
        position: Point::new(5.0, 0.0, 3.0),
        intensity: [1.0, 1.0, 1.0],
    };

    let reflectance = Reflectance {
        ambient: 0.2,
        diffuse: 0.4,
        specular: 0.4,
        specular_exponent: 10.0,
    };
    let objects = vec![
        Object::new(
            Mesh::new(Shape::Sphere, 0.01, 0.01),
            Material {
                color: [255, 0, 255],
                reflectance,
            },
            Matrix::I,
        ),
        Object::new(
            Mesh::new(Shape::Sphere, 0.01, 0.01),
            Material {
                color: [0, 255, 0],
                reflectance,
            },
            translate_x(-2.0),
        ),
        Object::new(
            Mesh::new(Shape::Sphere, 0.01, 0.01),
            Material {
                color: [255, 255, 0],
                reflectance,
            },
            translate_x(2.0),
        ),
    ];

    let mut window = GraphicsWindow::new(w, h);
    window.draw_faces(shade_faces(&objects, &camera, &light));
    window.save_image("images/image2a.png");

    let window = ray_trace(w, h, &objects, &camera, &light);
    window.save_image("images/image2b.png");
}

fn ray_trace(
    w: u32,
    h: u32,
    objects: &[Object],
    camera: &Camera,
    light: &LightSource,
) -> GraphicsWindow {
    let mut window = GraphicsWindow::new(w, h);
    for x in 0..w {
        for y in 0..h {
            if let Some(color) = pixel_color(&objects, &camera, &light, x, y) {
                window.draw_point((x as i32, y as i32), color);
            }
        }
    }
    window
}
