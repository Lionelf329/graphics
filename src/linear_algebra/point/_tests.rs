use crate::{Point, Vector};

#[test]
fn dot() {
    let p1 = Point::new(1.0, 2.0, 3.0);
    let p = Point::new(4.0, 5.0, 6.0);
    let v = Vector::new(4.0, 5.0, 6.0);
    assert_eq!(p1.dot(p), 32.0);
    assert_eq!(p1.dot(v), 32.0);
}

#[test]
fn middle() {
    let points = [Point::new(1.0, 2.0, 3.0), Point::new(3.0, 4.0, 5.0)];
    assert_eq!(
        Point::middle(&points).to_string(),
        Point::new(2.0, 3.0, 4.0).to_string()
    )
}

#[test]
pub fn to_string() {
    assert_eq!(
        Point::new(1.0, 2.0, 3.0).to_string(),
        "(+1.000, +2.000, +3.000, +1.000)"
    )
}

#[test]
pub fn add_vector() {
    let p = Point::new(1.0, 3.0, 5.0);
    let v = Vector::new(1.0, 2.0, 3.0);
    assert_eq!((p + v).to_string(), Point::new(2.0, 5.0, 8.0).to_string());
}

#[test]
pub fn sub_point() {
    let p1 = Point::new(1.0, 3.0, 5.0);
    let p2 = Point::new(1.0, 2.0, 3.0);
    assert_eq!(
        (p1 - p2).to_string(),
        Vector::new(0.0, 1.0, 2.0).to_string()
    );
}

#[test]
pub fn sub_vector() {
    let p = Point::new(1.0, 3.0, 5.0);
    let v = Vector::new(1.0, 2.0, 3.0);
    assert_eq!((p - v).to_string(), Point::new(0.0, 1.0, 2.0).to_string());
}

#[test]
pub fn add_assign_vector() {
    let mut p = Point::new(1.0, 3.0, 5.0);
    p += Vector::new(1.0, 2.0, 3.0);
    assert_eq!(p.to_string(), Point::new(2.0, 5.0, 8.0).to_string());
}

#[test]
pub fn sub_assign_vector() {
    let mut p = Point::new(1.0, 3.0, 5.0);
    p -= Vector::new(1.0, 2.0, 3.0);
    assert_eq!(p.to_string(), Point::new(0.0, 1.0, 2.0).to_string());
}

#[test]
pub fn index() {
    let mut p = Point::new(1.0, 2.0, 3.0);
    assert_eq!(&p[2], &3.0);
    assert_eq!(&mut p[2], &mut 3.0);
}
