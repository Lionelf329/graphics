use crate::{Vector, VectorOrPoint};

mod display;
mod operators;

#[cfg(test)]
mod _tests;

/// A point in 3 dimensional space.
///
/// This struct contains an array of 3 `f64` values, which represent a position in 3 dimensional
/// space. In most regards, it behaves as though it were a vector with 4 components, with the
/// following structure:
/// ```text
/// [X Y Z 1]
/// ```
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Point {
    pub(crate) values: [f64; 3],
}

impl Point {
    /// The location at the center of the coordinate system.
    pub const ORIGIN: Self = Self::new(0.0, 0.0, 0.0);

    /// Returns a `Point` constructed from x, y, and z coordinates.
    pub const fn new(x: f64, y: f64, z: f64) -> Self {
        Self { values: [x, y, z] }
    }

    /// Zero-cost conversion to a vector.
    ///
    /// This is equivalent to `self - Point::ORIGIN`.
    pub const fn to_vector(self) -> Vector {
        Vector {
            values: self.values,
        }
    }

    /// Returns the dot product between `self` and a `Point` or `Vector`.
    ///
    /// It uses only the first three components to compute the dot product, the 4th is ignored.
    pub fn dot<T: VectorOrPoint>(&self, other: T) -> f64 {
        self[0] * other[0] + self[1] * other[1] + self[2] * other[2]
    }

    /// Returns the point in the middle of all the points in the list.
    ///
    /// The middle is computed by adding all of the points together, and then dividing by how many
    /// of them there are.
    pub fn middle(points: &[Self]) -> Self {
        let mut ans = Self::ORIGIN;
        let recip = (points.len() as f64).recip();
        for c in 0..3 {
            for &p in points {
                ans[c] += p[c];
            }
            ans[c] *= recip;
        }
        ans
    }
}
