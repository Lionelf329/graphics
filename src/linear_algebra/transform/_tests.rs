use crate::transform::*;
use crate::{Matrix, Vector};

#[test]
pub fn test_rotate() {
    let m1 = rotate(Vector::new(1.0, 1.0, 1.0), 45.0_f64.to_radians());
    let m2 = Matrix {
        values: [
            [0.805, -0.311, 0.506, 0.000],
            [0.506, 0.805, -0.311, 0.000],
            [-0.311, 0.506, 0.805, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_rotate_x() {
    let m1 = rotate_x(45.0_f64.to_radians());
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 0.000],
            [0.000, 0.707, -0.707, 0.000],
            [0.000, 0.707, 0.707, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_rotate_y() {
    let m1 = rotate_y(45.0_f64.to_radians());
    let m2 = Matrix {
        values: [
            [0.707, 0.000, 0.707, 0.000],
            [0.000, 1.000, 0.000, 0.000],
            [-0.707, 0.000, 0.707, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_rotate_z() {
    let m1 = rotate_z(45.0_f64.to_radians());
    let m2 = Matrix {
        values: [
            [0.707, -0.707, 0.000, 0.000],
            [0.707, 0.707, 0.000, 0.000],
            [0.000, 0.000, 1.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_rotate_to() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 3.0, 1.0);
    let m = rotate_to(v1, v2);
    assert_eq!((m * v1).normalize().to_string(), v2.normalize().to_string());
}

#[test]
pub fn test_scale() {
    let m1 = scale(2.0, 3.0, 4.0);
    let m2 = Matrix {
        values: [
            [2.000, 0.000, 0.000, 0.000],
            [0.000, 3.000, 0.000, 0.000],
            [0.000, 0.000, 4.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_scale_x() {
    let m1 = scale_x(2.0);
    let m2 = Matrix {
        values: [
            [2.000, 0.000, 0.000, 0.000],
            [0.000, 1.000, 0.000, 0.000],
            [0.000, 0.000, 1.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_scale_y() {
    let m1 = scale_y(2.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 0.000],
            [0.000, 2.000, 0.000, 0.000],
            [0.000, 0.000, 1.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_scale_z() {
    let m1 = scale_z(2.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 0.000],
            [0.000, 1.000, 0.000, 0.000],
            [0.000, 0.000, 2.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_scale_all() {
    let m1 = scale_all(2.0);
    let m2 = Matrix {
        values: [
            [2.000, 0.000, 0.000, 0.000],
            [0.000, 2.000, 0.000, 0.000],
            [0.000, 0.000, 2.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_translate() {
    let m1 = translate(1.0, 2.0, 3.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 1.000],
            [0.000, 1.000, 0.000, 2.000],
            [0.000, 0.000, 1.000, 3.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_translate_x() {
    let m1 = translate_x(1.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 1.000],
            [0.000, 1.000, 0.000, 0.000],
            [0.000, 0.000, 1.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_translate_y() {
    let m1 = translate_y(1.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 0.000],
            [0.000, 1.000, 0.000, 1.000],
            [0.000, 0.000, 1.000, 0.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn test_translate_z() {
    let m1 = translate_z(1.0);
    let m2 = Matrix {
        values: [
            [1.000, 0.000, 0.000, 0.000],
            [0.000, 1.000, 0.000, 0.000],
            [0.000, 0.000, 1.000, 1.000],
        ],
    };
    assert_eq!(m1.to_string(), m2.to_string());
}
