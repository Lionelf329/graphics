use crate::Matrix;

/// Returns a matrix that translates in all three directions by given distances.
pub const fn translate(x: f64, y: f64, z: f64) -> Matrix {
    Matrix {
        values: [[1.0, 0.0, 0.0, x], [0.0, 1.0, 0.0, y], [0.0, 0.0, 1.0, z]],
    }
}

/// Returns a matrix that translates in the x direction by a given distance.
pub const fn translate_x(x: f64) -> Matrix {
    Matrix {
        values: [
            [1.0, 0.0, 0.0, x],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
        ],
    }
}

/// Returns a matrix that translates in the y direction by a given distance.
pub const fn translate_y(y: f64) -> Matrix {
    Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, y],
            [0.0, 0.0, 1.0, 0.0],
        ],
    }
}

/// Returns a matrix that translates in the z direction by a given distance.
pub const fn translate_z(z: f64) -> Matrix {
    Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, z],
        ],
    }
}
