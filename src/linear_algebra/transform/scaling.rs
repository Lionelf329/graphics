use crate::Matrix;

/// Returns a matrix that scales on each axis by a different factor.
pub const fn scale(x: f64, y: f64, z: f64) -> Matrix {
    Matrix {
        values: [[x, 0., 0., 0.], [0., y, 0., 0.], [0., 0., z, 0.]],
    }
}

/// Returns a matrix that scales on the x axis by a given factor.
pub const fn scale_x(x: f64) -> Matrix {
    Matrix {
        values: [
            [x, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
        ],
    }
}

/// Returns a matrix that scales on the y axis by a given factor.
pub const fn scale_y(y: f64) -> Matrix {
    Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, y, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
        ],
    }
}

/// Returns a matrix that scales on the z axis by a given factor.
pub const fn scale_z(z: f64) -> Matrix {
    Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, z, 0.0],
        ],
    }
}

/// Returns a matrix that scales on all three axes by a given factor.
pub const fn scale_all(n: f64) -> Matrix {
    Matrix {
        values: [[n, 0.0, 0.0, 0.0], [0.0, n, 0.0, 0.0], [0.0, 0.0, n, 0.0]],
    }
}
