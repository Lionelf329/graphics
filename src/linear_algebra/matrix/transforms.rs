use crate::transform::*;
use crate::{Matrix, Vector};

impl Matrix {
    /// Applies the transform to the current matrix.
    pub fn transform(self, t: Self) -> Self {
        t * self
    }

    /// Applies the `rotate` transform to the current matrix.
    pub fn rotate(self, t: Vector, a: f64) -> Self {
        rotate(t, a) * self
    }

    /// Applies the `rotate_x` transform to the current matrix.
    pub fn rotate_x(self, a: f64) -> Self {
        rotate_x(a) * self
    }

    /// Applies the `rotate_y` transform to the current matrix.
    pub fn rotate_y(self, a: f64) -> Self {
        rotate_y(a) * self
    }

    /// Applies the `rotate_z` transform to the current matrix.
    pub fn rotate_z(self, a: f64) -> Self {
        rotate_z(a) * self
    }

    /// Applies the `rotate_to` transform to the current matrix.
    pub fn rotate_to(self, from: Vector, to: Vector) -> Self {
        rotate_to(from, to) * self
    }

    /// Applies the `scale` transform to the current matrix.
    pub fn scale(self, x: f64, y: f64, z: f64) -> Self {
        scale(x, y, z) * self
    }

    /// Applies the `scale_x` transform to the current matrix.
    pub fn scale_x(self, x: f64) -> Self {
        scale_x(x) * self
    }

    /// Applies the `scale_y` transform to the current matrix.
    pub fn scale_y(self, y: f64) -> Self {
        scale_y(y) * self
    }

    /// Applies the `scale_z` transform to the current matrix.
    pub fn scale_z(self, z: f64) -> Self {
        scale_z(z) * self
    }

    /// Applies the `scale_all` transform to the current matrix.
    pub fn scale_all(self, n: f64) -> Self {
        scale_all(n) * self
    }

    /// Applies the `translate` transform to the current matrix.
    pub fn translate(self, x: f64, y: f64, z: f64) -> Self {
        translate(x, y, z) * self
    }

    /// Applies the `translate_x` transform to the current matrix.
    pub fn translate_x(self, x: f64) -> Self {
        translate_x(x) * self
    }

    /// Applies the `translate_y` transform to the current matrix.
    pub fn translate_y(self, y: f64) -> Self {
        translate_y(y) * self
    }

    /// Applies the `translate_z` transform to the current matrix.
    pub fn translate_z(self, z: f64) -> Self {
        translate_z(z) * self
    }
}
