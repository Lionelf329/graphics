use crate::{Matrix, Point, Vector};

mod getters;
mod setters;
mod transforms;

/// A collection of transforms between different coordinate systems.
#[derive(Copy, Clone, Debug)]
pub struct Camera {
    /// the camera's up direction.
    up: Vector,
    /// the location of the camera.
    position: Point,
    /// the location that the camera is looking at.
    gaze: Point,
    /// the viewport's width.
    width: f64,
    /// the viewport's height.
    height: f64,
    /// the camera's field of view.
    theta: f64,
    /// world to viewing matrix.
    w2v: Matrix,
    /// viewing to image matrix.
    v2i: Matrix,
    /// world to image matrix.
    w2i: Matrix,
    /// world to image matrix inverse.
    i2w: Matrix,
}

impl Camera {
    /// Returns a camera object constructed from information about the position and orientation of
    /// the camera, as well as properties of the viewport.
    pub fn new(
        up: Vector,
        position: Point,
        gaze: Point,
        width: u32,
        height: u32,
        theta: f64,
    ) -> Camera {
        let (width, height) = (width as f64, height as f64);
        let w2v = Camera::get_world_to_viewing(up, position, gaze);
        let v2i = Camera::get_viewing_to_image(width, height, theta);
        let w2i = v2i * w2v;
        let i2w = w2i.inverse();
        Camera {
            up,
            position,
            gaze,
            width,
            height,
            theta,
            w2v,
            v2i,
            w2i,
            i2w,
        }
    }

    /// Returns a matrix that rotates and translates to move from world to viewing coordinates.
    ///
    /// This transformation only does two things - it moves the origin, and it switches from using
    /// the xyz axes in world coordinates to three new perpendicular axes that are computed from
    /// the up, position, and gaze provided. The geometry of the scene is preserved.
    pub fn get_world_to_viewing(up: Vector, position: Point, gaze: Point) -> Matrix {
        let up = up.normalize();
        let n = (position - gaze).normalize();
        let u = up.cross(n).normalize();
        let v = n.cross(u).normalize();
        Matrix {
            values: [
                [u[0], u[1], u[2], -position.dot(u)],
                [v[0], v[1], v[2], -position.dot(v)],
                [n[0], n[1], n[2], -position.dot(n)],
            ],
        }
    }

    /// Returns a matrix that converts viewing coordinates to image coordinates.
    pub fn get_viewing_to_image(width: f64, height: f64, theta: f64) -> Matrix {
        // rescale height and width such that a projection surface centered at -1 along the z axis
        // that extends ±1 in the x and y directions will have the right field of view from top to
        // bottom, and the aspect ratio will be correct.
        let t = (theta / 2.0).tan();
        let r = t * width / height;
        let s1 = Matrix {
            values: [
                [1.0 / r, 0.0, 0.0, 0.0],
                [0.0, 1.0 / t, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
            ],
        };

        // Flip the y axis so the range goes from the top to bottom of the screen instead of bottom
        // to top, and move points from being like (1) to being like (2), where (1) and (2)
        // represent camera positions. This is accomplished by translating x and y based on their
        // depth. This moves the plane from [-1, 1] to [-2, 0].
        // \     /   \  |
        //  \   /     \ |
        //   \ /       \|
        //   (1)       (2)
        let t2 = Matrix {
            values: [
                [1.0, 0.0, -1.0, 0.0],
                [0.0, -1.0, -1.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
            ],
        };

        // Resize the plane to make it the same size as the viewport. This changes the x and y
        // ranges from [-2, 0] for both to [-w, 0] for x and [-h, 0] for y, where (0, 0) is the
        // top-left corner of the viewport.
        let s2 = Matrix {
            values: [
                [width / 2.0, 0.0, 0.0, 0.0],
                [0.0, height / 2.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
            ],
        };

        s2 * t2 * s1
    }

    /// Returns the CSS perspective property needed for the specified viewport.
    pub fn get_css_perspective(&self) -> f64 {
        self.height / 2.0 / (self.theta / 2.0).tan()
    }
}
