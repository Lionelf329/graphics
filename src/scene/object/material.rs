/// Color and lighting information for an object.
#[derive(Copy, Clone, Debug)]
pub struct Material {
    /// The material's color.
    pub color: Color,
    /// The lighting properties of the material.
    pub reflectance: Reflectance,
}

/// An RGB color.
pub type Color = [u8; 3]; // [red, green, blue]

/// Lighting properties of a material.
#[derive(Copy, Clone, Debug)]
pub struct Reflectance {
    /// The ambient light multiplier.
    pub ambient: f64,
    /// The diffuse light multiplier.
    pub diffuse: f64,
    /// The specular light multiplier.
    pub specular: f64,
    /// The specular light exponent.
    pub specular_exponent: f64,
}
