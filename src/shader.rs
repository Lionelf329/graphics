use crate::lighting::get_color;
use crate::{Camera, Color, LightSource, Object, Point};

/// Returns a list of (depth, vertices, color) tuples.
pub fn shade_faces(
    objects: &[Object],
    camera: &Camera,
    light: &LightSource,
) -> Vec<(f64, Vec<[f64; 2]>, Color)> {
    let mut face_list = vec![];
    let epsilon = 0.001;
    let lv = camera.world_to_viewing(light.position);
    for object in objects {
        let ((du, u_max), (dv, v_max)) = object.mesh.get_uv_range();
        let mut u = 0.0;
        while u + du < u_max + epsilon {
            let mut v = 0.0;
            while v + dv < v_max + epsilon {
                let points = [
                    camera.world_to_viewing(object.get_point(u, v).0),
                    camera.world_to_viewing(object.get_point(u + du, v).0),
                    camera.world_to_viewing(object.get_point(u + du, v + dv).0),
                    camera.world_to_viewing(object.get_point(u, v + dv).0),
                ];
                v += dv;
                if points.iter().any(|p| p[2] > 0.0) {
                    continue;
                }

                let c = Point::middle(&points);
                let n = (points[0] - points[2])
                    .cross(points[1] - points[3])
                    .normalize();
                let s = (lv - c).normalize();
                if let (color, true) = get_color(
                    n,
                    -c.to_vector().normalize(),
                    s,
                    light.intensity,
                    &object.material,
                    false,
                ) {
                    let pixel_coords = vec![
                        camera.viewing_to_pixel(points[0]),
                        camera.viewing_to_pixel(points[1]),
                        camera.viewing_to_pixel(points[2]),
                        camera.viewing_to_pixel(points[3]),
                    ];
                    face_list.push((c[2], pixel_coords, color))
                }
            }
            u += du;
        }
    }
    face_list
}
