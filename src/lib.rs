//! This crate contains a collection of useful utilities for generating computer graphics.

#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

pub use linear_algebra::*;
pub use ray_tracer::pixel_color;
pub use scene::*;
pub use shader::shade_faces;

mod lighting;
mod linear_algebra;
mod ray_tracer;
mod scene;
mod shader;
mod solvers;
